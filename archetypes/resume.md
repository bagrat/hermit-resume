---
type: "{{with $splslice := split .Name "-" }}{{ index $splslice 0 }}{{end}}"

# General
name: "{{ replace .Name "-" " " | strings.TrimPrefix "job " | strings.TrimPrefix "edu " | strings.TrimPrefix "tech " | strings.TrimPrefix "mooc " | title }}"
link: "https://"
date: 0
endDate: 0

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: ""
location: ""
info: ""
---

